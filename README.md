# Source Insight 4.0 暗黑系（护眼）主题UI

## 简介

本仓库提供了一个专为Source Insight 4.0设计的暗黑系（护眼）主题UI资源文件。该主题旨在提供一个舒适的编程环境，减少长时间编程对眼睛的疲劳。

## 特点

- **暗黑系设计**：采用深色背景和浅色文字，减少屏幕亮度对眼睛的刺激。
- **护眼模式**：优化颜色搭配，减少蓝光，保护视力。
- **自定义选项**：用户可以根据个人喜好调整颜色和字体设置。

## 使用方法

1. **下载资源文件**：
   - 点击仓库中的`SourceInsight_DarkTheme.zip`文件进行下载。

2. **导入主题**：
   - 解压下载的`SourceInsight_DarkTheme.zip`文件。
   - 打开Source Insight 4.0。
   - 进入`Options` -> `Load Configuration`。
   - 选择解压后的主题文件进行导入。

3. **应用主题**：
   - 在Source Insight中，进入`Options` -> `Preferences` -> `Colors`。
   - 选择导入的主题并应用。

## 贡献

欢迎大家提出改进建议或提交新的主题样式。请通过提交Issue或Pull Request的方式参与贡献。

## 许可证

本项目采用MIT许可证，详情请参阅[LICENSE](LICENSE)文件。

## 联系我们

如有任何问题或建议，请通过[GitHub Issues](https://github.com/yourusername/yourrepository/issues)联系我们。

---

希望这个主题能为你的编程工作带来更好的体验！